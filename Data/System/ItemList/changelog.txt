Changelog

(weeb_tv: Hope I didn't mess anything up!)
	- Every Grade 3 (Blue)/Grade 4 (Orange) weapon has been made equal to its Grade 2 (Green) counterpart in stats.
			- This includes everything, as well as the removal of dot damage
	- All dot_damage set to 0.
	- All dot_piercing set to 0.
	- All tooltips updated to reflect this as well.
	
	Notes
	- Rose Grade 4s (Oranges) remain untouched since they lack a Grade 2 counterpart. Includes their DoTs.
	- Minigun_undertaker is an NPC item, hence it was not changed
	- All GM weapons remain untouched. All x-mas weapons remain untouched.
	- Shotgun_Basic_Gold was missing one entry initially. Added the missing one - noted just in case.